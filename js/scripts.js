$(document).ready(function() {

    $(window).bind('scroll', function () {
        if ($(window).scrollTop() > 0) {
            $("#container-menu").addClass('menu-prueba');
        }else{
            $("#container-menu").removeClass('menu-prueba');
        }
    });


// SCROLL HACIA ABAJO
    var scrollLink = $('.scroll');
    scrollLink.click(function(e) {
    	e.preventDefault();
    	$('body,html').animate({
    		scrollTop: $(this.hash).offset().top - 60
    	}, 500 );
    });


// MARCAR EL MENU

  $(window).scroll(function() {	
      var scrollbarLocation = $(this).scrollTop();
      
      scrollLink.each(function() {
      	var sectionOffset = $(this.hash).offset().top - 100;
      	if ( sectionOffset <= scrollbarLocation ) {
      		$(this).parent().addClass('active');
      		$(this).parent().siblings().removeClass('active');
      	}
      });
  });


  //S

/* LOADER*/

    $(window).on('load', function () {
        $("#loader").delay(2000).fadeOut("slow");
        $('body').css('overflow', 'visible');
    });

/*SLIDER*/

    $(".Modern-Slider").slick({
        autoplay:true,
        autoplaySpeed:10000,
        speed:600,
        slidesToShow:1,
        slidesToScroll:1,
        pauseOnHover:false,
        dots:true,
        pauseOnDotsHover:true,
        cssEase:'linear',
       // fade:true,
        draggable:false,
        prevArrow:'<button class="PrevArrow"></button>',
        nextArrow:'<button class="NextArrow"></button>', 
    });

/*ANIMACION SCROLL*/
    new WOW().init();




});
















